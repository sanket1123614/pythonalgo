import math
from flask import Flask, request, jsonify
from scipy.signal import find_peaks
import pandas as pd
import numpy as np

app = Flask(__name__)


def euler_from_quaternion(x, y, z, w):
    """
    Convert a quaternion into euler angles (roll, pitch, yaw)
    roll is rotation around x in radians (counterclockwise)
    pitch is rotation around y in radians (counterclockwise)
    yaw is rotation around z in radians (counterclockwise)
    """
    t0 = +2.0 * (w * x + y * z)
    t1 = +1.0 - 2.0 * (x * x + y * y)
    roll_x = math.atan2(t0, t1)

    t2 = +2.0 * (w * y - z * x)
    t2 = +1.0 if t2 > +1.0 else t2
    t2 = -1.0 if t2 < -1.0 else t2
    pitch_y = math.asin(t2)

    t3 = +2.0 * (w * z + x * y)
    t4 = +1.0 - 2.0 * (y * y + z * z)
    yaw_z = math.atan2(t3, t4)

    return roll_x, pitch_y, yaw_z  # in radians


def modify_data(df):
    # df['ax'] = 0
    # df['ay'] = 0
    # df['az'] = 0
    # df['tas'] = 0

    # df['gx'] = 0
    # df['gy'] = 0
    # df['gz'] = 0

    df['qx'] = 0
    df['qy'] = 0
    df['qz'] = 0
    df['qw'] = 0

    df['yaw'] = 0
    df['pitch'] = 0
    df['roll'] = 0

    for ind,_ in df.iterrows():
        # df.at[ind,'ax'] = df['accel'].iloc[ind][0]
        # df.at[ind,'ay'] = df['accel'].iloc[ind][1]
        # df.at[ind,'az'] = df['accel'].iloc[ind][2]

        # df.at[ind,'gx'] = df['gyro'].iloc[ind][0]
        # df.at[ind,'gy'] = df['gyro'].iloc[ind][1]
        # df.at[ind,'gz'] = df['gyro'].iloc[ind][2]

        df["qw"].iloc[ind] = df["quat"].iloc[ind][0]
        df["qx"].iloc[ind] = df["quat"].iloc[ind][1]
        df["qy"].iloc[ind] = df["quat"].iloc[ind][2]
        df["qz"].iloc[ind] = df["quat"].iloc[ind][3]

        df.at[ind,'yaw'],df.at[ind,'pitch'],df.at[ind,'roll'] = list(map(lambda x : x*57.2958,euler_from_quaternion(df.at[ind,'qx'],df.at[ind,'qy'],df.at[ind,'qz'],df.at[ind,'qw'])))

        # if(ind):
        #     df.at[ind,'tas'] = df['totalAccel'].iloc[ind] - df['totalAccel'].iloc[ind-1]

    # df.drop(columns='accel',inplace=True)
    # df.drop(columns='gyro',inplace=True)
    df.drop(columns='quat',inplace=True)
    return df


def drop_calculation(df):
    try:
        first_trough = find_peaks((-1*df['totalAccel'].to_numpy()),height=-0.1)[0][0]
    except:
        first_trough = 0

    try:
        first_peak = [x for x,y in df['totalAccel'].items() if y > 2.5 and x > first_trough][0]
    except:
        first_peak = 0
    # print(first_trough,first_peak)

    init_peaks = find_peaks((df['totalAccel'].to_numpy()),height=0.9)[0][:]

    try:
        init_time = max([x for x in init_peaks if x < first_trough])
    except:
        init_time = 0
    # print(init_time)

    true_peaks = []
    true_troughs = []
    peaks = find_peaks(df['totalAccel'].to_numpy(),height=3,distance=4)[0]
    troughs = find_peaks((-1*df['totalAccel'].to_numpy()),height=-0.4)[0]
    # print(peaks)
    # print(peaks.tolist())

    true_peaks.append(first_peak)
    true_troughs.append(init_time)
    # true_troughs.append(first_trough)

    init_dur = (true_peaks[0] - true_troughs[0])

    if(len(peaks) >= 2):
        for i in range(len(peaks)-1):
            mid_troughs = [x for x in troughs if x > peaks[i] and x < peaks[i+1]]
            if(len(mid_troughs)):
                # dep_trough = mid_troughs[-1]
                dep_trough = mid_troughs[0]
                dep_peak = [x for x,y in df['totalAccel'].items() if y > 2.5 and x > dep_trough][0]
                # dif_time = (peaks[i+1]-dep_trough)
                dif_time = (dep_peak-dep_trough)
                # if(dif_time <= init_dur):
                if(dif_time <= (true_peaks[-1]-true_troughs[-1])):
                    # true_peaks.append(peaks[i+1])
                    true_peaks.append(dep_peak)
                    true_troughs.append(dep_trough)

    # print(true_peaks)
    # print(true_troughs)

    heights = []
    for x,y in zip(true_troughs,true_peaks):
        height = 0.5*9.8*(y-x)*(y-x)*100*100/(1000*1000)
        if (height > 0.1):
            # print(x,y) 
            heights.append(round(height,3))

    # print(heights) #in cm
    print(f"Number of Bounces : {len(heights)} || heights :: ",end=" ")
    for height in heights:
        print(f"{height}",end=",")
    print()

    ypr_start = np.array([df['yaw'].iloc[0],df['pitch'].iloc[0],df['roll'].iloc[0]])
    ypr_first_trough = np.array([df['yaw'].iloc[first_trough],df['pitch'].iloc[first_trough],df['roll'].iloc[first_trough]])
    ypr_first_peak = np.array([df['yaw'].iloc[first_peak+1],df['pitch'].iloc[first_peak+1],df['roll'].iloc[first_peak+1]])

    entry_angle = np.mean(np.absolute(ypr_first_trough-ypr_start))
    exit_angle = np.mean(np.absolute(ypr_first_peak-ypr_start))

    while(entry_angle >= 180):
        entry_angle = entry_angle%180
    if(entry_angle >=90):
        entry_angle -=90

    while(exit_angle >= 180):
        exit_angle = exit_angle%180
    if(exit_angle >=90):
        exit_angle -=90
    
    entry_angle = round(entry_angle,2)
    exit_angle = round(exit_angle,2)

    res = {'heights':heights,'peaks':peaks.tolist(),'angles':[entry_angle,exit_angle]}
    return res

    # return heights


@app.route("/")
def home():
    return jsonify("Home"), 200


@app.route("/getBounces/<id>")
def get_data(id):
    extra = request.args.get("extra")
    return jsonify(extra), 200


@app.route("/getBounces", methods=["POST"])
def get_bounces():
    try:
        data = request.get_json()
        newData = {
            "totalAccel": pd.Series(data["totalAccel"]),
            "quat": pd.Series(data["quat"]),
            # "euler": pd.Series(data["euler"]),
            # "accel": pd.Series(data["accel"]),
            # "gyro": pd.Series(data["gyro"]),
        }
        dataframe = pd.concat(newData, axis=1)
        mdata = modify_data(dataframe)
        response = drop_calculation(mdata)
        # print('hello')
        # print(response)
        return jsonify({"status": True, "data": response}), 200
    except Exception as e:
        print("The error is: ",e)
        return jsonify({"status": False, "data": "Invalid Input!"}), 200


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=9000)


# source env bin activate
# pip3 install flask pandas scipy numpy
# nohup python3 main.py > log.txt 2>&1 &

# ss -tnlp | grep ""   (Check ongoing process)
# kill process id